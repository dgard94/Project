const configDir = './config/webpack/';
const appConfigDir = './config/application/';
const merge = require('webpack-merge');
const fileExists = require('file-exists');
const baseConfig = require(`${configDir}webpack.base.config.js`);
const scanFolder = require('scan-folder');
const path = require('path');
const fs = require('fs');

module.exports = function(env) {
  const environment = env || process.env.NODE_ENV || 'local';
  const privateConfigsPath = scanFolder(`${configDir}local`);
  const privateConfig = privateConfigsPath.length ? require(privateConfigsPath[0]) : false;
  const envConfig = fileExists.sync(`${configDir}webpack.${environment}.config.js`)
    ? require(`${configDir}webpack.${environment}.config.js`)
    : require(`${configDir}webpack.local.config.js`);

  if (process.argv[1].indexOf('eslint') === -1) {
    console.log('Environment: ', environment);
    console.log('Webpack Config File:', privateConfig && environment === 'local' ? path.basename(privateConfigsPath[0]) : `webpack.${environment}.config.js`);
    console.log('Application Config File: ', `application.${environment}.js`);
  }

  return merge(baseConfig, privateConfig && environment === 'local' ? privateConfig : envConfig);
};
