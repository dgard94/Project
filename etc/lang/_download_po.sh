#!/usr/bin/env bash
projectID="6"
resourceID="332"
path="$1"

if [ -z "$path" ]; then exit 2; fi

wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/91/export/" -O "${path}"/zh.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/13/export/" -O "${path}"/cz.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/20/export/" -O "${path}"/en.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/28/export/" -O "${path}"/fr.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/16/export/" -O "${path}"/de.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/38/export/" -O "${path}"/hu.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/43/export/" -O "${path}"/it.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/68/export/" -O "${path}"/pl.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/72/export/" -O "${path}"/ru.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/23/export/" -O "${path}"/es.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/80/export/" -O "${path}"/sv.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/86/export/" -O "${path}"/tr.po
wget "http://ui.transifex.template-help.com/project/$projectID/resource/$resourceID/language/88/export/" -O "${path}"/uk.po
