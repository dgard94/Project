import React from 'react';
import { translate } from 'react-i18next';
import PropTypes from 'prop-types';
import moment from 'moment';
import MenuItem from './MenuItem';
import './menu.pcss';

const Menu = ({ t }) => (
  <div className="component-Menu">
    <h1>{t('Menu')}</h1>
    <h2>
      {t('%s at %s', {
        postProcess: 'sprintf',
        sprintf: [moment().format('LT'), moment().format('LL')],
      })}
    </h2>
    <MenuItem />
    <img src="images/favicon-16x16.png" alt="QWE" />
    <span>{moment().format('LT at LL')}</span>
  </div>
);
Menu.propTypes = { t: PropTypes.func.isRequired };

export default translate(['translation'])(Menu);
