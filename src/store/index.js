import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';
import reducer from 'reducers';

const devtools = '__REDUX_DEVTOOLS_EXTENSION__';

let args;

switch (process.env.NODE_ENV) {
  case 'production':
    args = compose(applyMiddleware(thunkMiddleware));
    break;
  default:
  case 'development':
    args = compose(applyMiddleware(thunkMiddleware, createLogger()));
    break;
  case 'local':
    args = compose(applyMiddleware(thunkMiddleware), window[devtools] && window[devtools]());
    break;
}

export default createStore(reducer, args);
