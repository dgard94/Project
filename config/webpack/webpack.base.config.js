require('postcss-svg');
require('postcss-import');
require('postcss-partial-import');
require('postcss-mixins');
require('postcss-simple-vars');
require('postcss-conditionals');
require('postcss-custom-media');
require('postcss-media-minmax');
require('postcss-color-function');
require('postcss-nested');
require('postcss-property-lookup');
require('postcss-extend');
require('autoprefixer');
require('postcss-discard-empty');

const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const baseDir = process.cwd();
const nodeModulesPath = resolve(baseDir, 'node_modules');
const buildPath = resolve(baseDir, 'build');
const env = process.env.NODE_ENV || 'development';

module.exports = {
  context: resolve(__dirname, `${baseDir}/src`),
  output: {
    path: buildPath,
    filename: 'index.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css', '.pcss'],
    modules: [
      resolve(baseDir, ''),
      resolve(baseDir, `${baseDir}`),
      resolve(baseDir, `${baseDir}/src`),
      resolve(baseDir, `${baseDir}/static`),
      nodeModulesPath,
    ],
    alias: {
      'config.js': resolve(baseDir, `config/application/application.${env}.js`),
    },
  },
  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  module: {
    rules: [
      {
        test: /\.po$/,
        loaders: ['i18next-po-loader'],
      },
      {
        test: /\.js(x?)$/,
        exclude: [nodeModulesPath],
        use: ['babel-loader'],
      },
      {
        test: /\.(png)(\?[a-z0-9=&.]+)?$/,
        include: [resolve(baseDir, '../src/static/fonts')],
        use: 'raw-loader',
        exclude: [nodeModulesPath],
      },
      {
        test: /\/icon\/.*\.svg$/,
        loaders: [
          'svg-sprite-loader',
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                { removeTitle: true },
                { convertColors: { shorthex: false } },
                { convertPathData: false },
                { cleanupAttrs: true },
                { removeUselessStrokeAndFill: true },
                { removeNonInheritableGroupAttrs: true },
                { collapseGroups: true },
                { convertShapeToPath: true },
                { removeScriptElement: true },
                { removeEmptyContainers: true },
                { removeHiddenElems: true },
                { moveGroupAttrsToElems: true },
                // { transformsWithOnePath: true },
                // { removeAttrs: { attrs: '(fill|stroke)' }},
              ],
            },
          },
        ],
      },
      {
        test: /\/monoicon\/.*\.svg$/,
        loaders: [
          'svg-sprite-loader',
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                { removeTitle: true },
                { removeStyleElement: true },
                { removeScriptElement: true },
                { moveGroupAttrsToElems: true },
                { removeHiddenElems: true },
                { convertShapeToPath: true },
                { convertPathData: true },
                { removeUselessStrokeAndFill: true },
                { removeNonInheritableGroupAttrs: true },
                { removeAttrs: { attrs: '(fill|stroke|fill-opacity|stroke-opacity)' } },
                { removeEmptyContainers: true },
                { collapseGroups: true },
                { transformsWithOnePath: true },
              ],
            },
          },
        ],
      },
      {
        test: /\/images\/.*\.svg$/,
        use: 'file-loader',
      },
      {
        test: /\.(woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'null-loader',
      },
      {
        test: /\.pug$/,
        use: ['pug-loader'],
      },
    ],
  },
  plugins: [
    new TransferWebpackPlugin([{ from: `${baseDir}/static`, to: '.' }]),
    new HtmlWebpackPlugin({
      filename: './index.html',
      template: `${baseDir}/index.pug`,
      minify: {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        minifyJS: true,
        minifyCSS: true,
        removeAttributeQuotes: true,
        removeComments: true,
        removeEmptyAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
      },
    }),
  ],
};
