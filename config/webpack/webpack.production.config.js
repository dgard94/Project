const webpack = require('webpack');
const { resolve } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const baseDir = process.cwd();

module.exports = {
  devtool: false,
  entry: [
    'babel-polyfill',
    'app.jsx',
  ],
  output: {
    path: resolve(baseDir, 'build/'), // absolute path to bundle
    publicPath: '/',
    filename: 'index-[hash].js', // bundle name
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: true,
              sourceMap: false,
              root: '/vendors/',
              localIdentName: '[local]',
            },
          },
          'postcss-loader',
          { loader: 'less-loader' },
        ],
      },
      {
        test: /\.css$/,
        exclude: resolve(baseDir, 'node_modules/quark/css/main.css'),
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { importLoaders: 1 } },
            'postcss-loader',
          ],
        }),
      },
      {
        test: /\.pcss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { importLoaders: 1 } },
            'postcss-loader',
          ],
        }),
      },
      {
        test: resolve(baseDir, 'node_modules/quark/css/main.css'),
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
    }),
    new ExtractTextPlugin('index-[hash].css'),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        sequences: true,
        dead_code: true,
        conditionals: true,
        booleans: true,
        unused: true,
        if_return: true,
        join_vars: true,
      },
      mangle: {
        except: ['$super', '$', 'exports', 'require'],
      },
      output: {
        comments: false,
      },
    }),
  ],
};
