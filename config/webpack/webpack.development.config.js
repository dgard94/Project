const webpack = require('webpack');
const { resolve } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const baseDir = process.cwd();
const buildPath = resolve(baseDir, 'build');


module.exports = {
  devtool: '[inline-]source-map',
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './app.jsx',
  ],

  devServer: {
    contentBase: buildPath,
    port: 8080,
    host: '0.0.0.0',
    historyApiFallback: true,
  },

  output: {
    path: resolve(__dirname, '../build', 'build'),
    publicPath: buildPath,
    filename: 'index.js',
  },

  plugins: [
    new webpack.DefinePlugin({ 'process.env': { NODE_ENV: JSON.stringify('development') } }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new ExtractTextPlugin('index.css'),
  ],

  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              url: false,
              modules: true,
              sourceMap: false,
              root: '/vendors/',
              localIdentName: '[local]',
            },
          },
          { loader: 'postcss-loader' },
          { loader: 'less-loader' },
        ],
      },
      {
        test: /\.css$/,
        exclude: resolve(baseDir, 'node_modules/quark/css/main.css'),
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { importLoaders: 1 } },
            { loader: 'postcss-loader' },
          ],
        }),
      },
      {
        test: /\.pcss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { importLoaders: 1 } },
            'postcss-loader',
          ],
        }),
      },
      {
        test: resolve(baseDir, 'node_modules/quark/css/main.css'),
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
};
